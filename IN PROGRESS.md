General:
--------
* Implement the remaining Caps we might have missed
* IWC / HG implementation (even when it really basic)
* Redesign the WebUI to use the WebAPI

Scripting:
----------
* Upgrade DotNetEngine with latest OSSL / LSL / WhiteCore functions
* New scriptengine Phlox (Apache 2 License by InWorldz) to be integrated by Fine (March 2016)

Physics:
--------
* Upgrade Physics engine (Bullet 2.82 and perhaps PhysX for Windows machines)
